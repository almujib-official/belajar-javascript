let data, buffer;

/*
Method random mengembalikan float seperti:
0.24389694512955717
0.19602460529974985
0.38714676315912566
0.780186961653913
*/
data = Math.random();
console.log(data);
data = Math.random();
console.log(data);
data = Math.random();
console.log(data);
data = Math.random();
console.log(data);

console.log("=".repeat(10));

// Mengembalikan angka 0 - (buffer - 1).
// Apabila menginginkan angka 1 - buffer;
// maka tambahkan + 1.
buffer = 10;
data = Math.floor(Math.random() * buffer);
console.log(data);
data = Math.floor(Math.random() * buffer);
console.log(data);
data = Math.floor(Math.random() * buffer);
console.log(data);
data = Math.floor(Math.random() * buffer);
console.log(data);
data = Math.floor(Math.random() * buffer) + 1;
console.log(data);

console.log("=".repeat(10));